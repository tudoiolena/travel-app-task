import { useEffect, useState } from "react";
import "./App.css";
import {
  AddTripButton,
  CountdownTimer,
  Loader,
  ScrollableTripCards,
  Search,
  TodayWeatherSidebar,
  TripModal,
  WeatherForecast,
} from "./components";
import tripsData from "./trips.json";
import { Trip } from "./types/trip";
import {
  fetchWeatherPeriodForecast,
  fetchWeatherTodayForecast,
} from "./utils/api-call";
import { getWeatherIconUrl } from "./utils/icon-mapper";
import { ForecastDay } from "./interfaces/forecast.response";
import { TodayWeatherDay } from "./interfaces/today-weather.response";
import { CredentialResponse, GoogleLogin } from "@react-oauth/google";
import { JwtPayload, jwtDecode } from "jwt-decode";
import { LoginSocialFacebook, IResolveParams } from "reactjs-social-login";
import { FacebookLoginButton } from "react-social-login-buttons";

const App = () => {
  const staticTrip = {
    id: "d15b8374-3era-4624-8e38-5ecd11bv07f5",
    city: "Berlin",
    image:
      "trip-pictures/Berlin_Brandenburger_Tor_im_Sonnenuntergang_Leitmotiv_German_Summer_Cities.jpg",
    startDate: "14.03.2024",
    endDate: "21.03.2024",
  };

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [trips, setTrips] = useState<Trip[]>(() => {
    const storedTrips = localStorage.getItem("trips");
    return storedTrips ? JSON.parse(storedTrips) : [staticTrip];
  });
  const [selectedTrip, setSelectedTrip] = useState<Trip | null>(null);
  const [forecastData, setForecastData] = useState<ForecastDay[]>([]);
  const [todayWeatherData, setTodayWeatherData] = useState<TodayWeatherDay>();
  const [loadingTodayWeather, setLoadingTodayWeather] = useState(false);
  const [pictureUrl, setPictureUrl] = useState<string | null>(null);
  const [facebookProfile, setFacebookProfile] = useState<
    typeof IResolveParams | null
  >(null);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [filteredTrips, setFilteredTrips] = useState<Trip[]>([]);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const addTrip = (newTrip: Trip) => {
    setTrips((prevTrips) => {
      const updatedTrips = [...prevTrips, newTrip];
      localStorage.setItem("trips", JSON.stringify(updatedTrips));
      return updatedTrips;
    });
  };

  const formatDateDotsToDashes = (dateString: string): Date => {
    const [day, month, year] = dateString.split(".");
    return new Date(`${year}-${month}-${day}`);
  };

  const formatDateToIsoDateString = (dateString: string): string => {
    const dateObject = formatDateDotsToDashes(dateString);
    const isoDateString = dateObject.toISOString().split("T")[0];
    return isoDateString;
  };

  const handleTripClick = async (trip: Trip) => {
    setSelectedTrip(trip);

    const weatherForecastData = await fetchWeatherPeriodForecast(
      trip.city,
      formatDateToIsoDateString(trip.startDate),
      formatDateToIsoDateString(trip.endDate)
    );
    setForecastData(weatherForecastData.days);

    const todayWeatherData = await fetchWeatherTodayForecast(trip.city);
    setTodayWeatherData(todayWeatherData.days[0]);
  };

  const renderWeatherForecasts = () => {
    if (!forecastData) return <Loader />;

    return forecastData.map((day) => (
      <WeatherForecast
        key={day.datetime}
        weekday={new Date(day.datetime).toLocaleDateString("en-US", {
          weekday: "long",
        })}
        icon={getWeatherIconUrl(day.icon)}
        temperature={`${Math.round(day.tempmin)}°/${Math.round(day.tempmax)}°`}
      />
    ));
  };

  const renderTodayWeather = () => {
    if (loadingTodayWeather) {
      return <Loader />;
    }

    if (todayWeatherData) {
      return (
        <TodayWeatherSidebar
          weekday={new Date(todayWeatherData.datetime).toLocaleDateString(
            "en-US",
            {
              weekday: "long",
            }
          )}
          icon={getWeatherIconUrl(todayWeatherData.icon) || ""}
          temperature={`${Math.round(todayWeatherData.temp)}`}
          city={selectedTrip?.city || ""}
        />
      );
    }

    return null;
  };

  const sortedTrips = trips.slice().sort((a, b) => {
    const startDateA = formatDateDotsToDashes(a.startDate).getTime();
    const startDateB = formatDateDotsToDashes(b.startDate).getTime();
    return startDateA - startDateB;
  });

  const handleSuccessLogin = (credentialResponse: CredentialResponse) => {
    if (credentialResponse.credential) {
      const decoded = jwtDecode<{ picture: string } & JwtPayload>(
        credentialResponse.credential
      );
      const url = decoded.picture;
      setPictureUrl(url);
    }
  };

  const handleSearch = () => {
    if (searchTerm.trim().length === 0) {
      setFilteredTrips([]);
    } else {
      const filteredTrips = trips.filter((trip) =>
        trip.city.toLowerCase().includes(searchTerm.toLowerCase())
      );
      setFilteredTrips(filteredTrips);
    }
  };

  const arraysAreEqual = (arr1: Array<Trip>, arr2: Array<Trip>) => {
    return (
      arr1.length === arr2.length &&
      arr1.every((value, index) => value === arr2[index])
    );
  };

  useEffect(() => {
    if (searchTerm.trim().length === 0) {
      if (!arraysAreEqual(filteredTrips, sortedTrips)) {
        setFilteredTrips(sortedTrips);
      }
    }
  }, [searchTerm, sortedTrips, filteredTrips]);

  useEffect(() => {
    localStorage.setItem("trips", JSON.stringify(trips));
  }, [trips]);

  useEffect(() => {
    const fetchData = async () => {
      if (selectedTrip) {
        setLoadingTodayWeather(true);
        const todayWeatherData = await fetchWeatherTodayForecast(
          selectedTrip.city
        );
        setTodayWeatherData(todayWeatherData.days[0]);
        setLoadingTodayWeather(false);
      }
    };

    fetchData();
  }, [selectedTrip]);

  return (
    <div className="page-container">
      <div className="container">
        <div>
          <h1>
            Weather <strong>Forecast</strong>
          </h1>
          <Search
            value={searchTerm}
            onChange={(event) => setSearchTerm(event.target.value)}
            onSearch={handleSearch}
          />
          <div className="google-login__button">
            {!pictureUrl && (
              <GoogleLogin
                onSuccess={handleSuccessLogin}
                onError={() => {
                  console.error("Google Login Failed");
                }}
              />
            )}
            {pictureUrl && (
              <div className="google-photo-container">
                <img
                  className="google-photo"
                  src={pictureUrl}
                  alt="User Photo"
                />
              </div>
            )}
          </div>
          <div className="facebook-login__button">
            {!facebookProfile && (
              <LoginSocialFacebook
                appId="378852024458176"
                onResolve={({ data }: typeof IResolveParams) => {
                  setFacebookProfile(data);
                }}
                onReject={() => {
                  console.error("Facebook Login Failed");
                }}
              >
                <FacebookLoginButton
                  style={{ fontSize: "14px", height: "38px" }}
                />
              </LoginSocialFacebook>
            )}

            {facebookProfile && (
              <div className="facebook-container">
                You are logged in, {facebookProfile.name}
              </div>
            )}
          </div>
          <div className="trips-container">
            {searchTerm.trim().length > 0 ? (
              <ScrollableTripCards
                trips={filteredTrips}
                onTripClick={handleTripClick}
              />
            ) : (
              <ScrollableTripCards
                trips={sortedTrips}
                onTripClick={handleTripClick}
              />
            )}
            <AddTripButton onClick={openModal} />
          </div>
          <TripModal
            isOpen={isModalOpen}
            onClose={closeModal}
            cities={tripsData}
            onAddTrip={addTrip}
          />
          {selectedTrip && (
            <div>
              <h2 className="weather-forecast__title">Week</h2>
              {renderWeatherForecasts()}
            </div>
          )}
        </div>
        {selectedTrip && (
          <div className="sidebar">
            <div className="sidebar__content">
              {renderTodayWeather()}
              <CountdownTimer startDate={selectedTrip.startDate} />
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default App;
