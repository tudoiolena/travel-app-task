export const getWeatherIconUrl = (iconCode: string) => {
  const iconUrls =
    "https://raw.githubusercontent.com/visualcrossing/WeatherIcons/main/PNG/3rd%20Set%20-%20Color/";

  const iconMap: Record<string, string> = {
    "clear-day": "clear-day.png",
    "clear-night": "clear-night.png",
    cloudy: "cloudy.png",
    fog: "fog.png",
    hail: "hail.png",
    "partly-cloudy-day": "partly-cloudy-day.png",
    "partly-cloudy-night": "partly-cloudy-night.png",
    "rain-snow-showers-day": "rain-snow-showers-day.png",
    "rain-snow-showers-night": "rain-snow-showers-night.png",
    "rain-snow": "rain-snow.png",
    rain: "rain.png",
    "showers-day": "showers-day.png",
    "showers-night": "showers-night.png",
    sleet: "sleet.png",
    "snow-showers-day": "snow-showers-day.png",
    "snow-showers-night": "snow-showers-night.png",
    snow: "snow.png",
    "thunder-rain": "thunder-rain.png",
    "thunder-showers-day": "thunder-showers-day.png",
    "thunder-showers-night": "thunder-showers-night.png",
    thunder: "thunder.png",
    wind: "wind.png",
  };
  if (iconCode in iconMap) {
    return `${iconUrls}${iconMap[iconCode]}`;
  }
};
