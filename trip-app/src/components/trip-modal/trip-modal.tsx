import {
  CSSProperties,
  ChangeEvent,
  FC,
  FormEvent,
  useEffect,
  useState,
} from "react";
import { Trip } from "../../types/trip";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onAddTrip: (newTrip: Trip) => void;
  cities: { id: string; city: string; image: string }[];
}

export const TripModal: FC<IProps> = ({
  isOpen,
  onClose,
  cities,
  onAddTrip,
}) => {
  const [selectedCity, setSelectedCity] = useState("");
  const [selectedCityImage, setSelectedCityImage] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const modalStyle: CSSProperties = {
    display: isOpen ? "flex" : "none",
  };

  const handleCityChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const newCity = event.target.value;
    setSelectedCity(newCity);

    const selectedCityData = cities.find((city) => city.city === newCity);
    if (selectedCityData) {
      setSelectedCityImage(selectedCityData.image);
    } else {
      setSelectedCityImage("");
    }
  };

  const formatDate = (dateString: string): string => {
    const date = new Date(dateString);
    const options = {
      day: "numeric" as const,
      month: "numeric" as const,
      year: "numeric" as const,
    };
    return date.toLocaleDateString("en-GB", options).replace(/\//g, ".");
  };

  const handleStartDateChange = (event: ChangeEvent<HTMLInputElement>) => {
    const selectedDate = new Date(event.target.value);
    const today = new Date();
    const maxAllowedDate = new Date();
    maxAllowedDate.setDate(today.getDate() + 15);

    if (selectedDate >= today && selectedDate <= maxAllowedDate) {
      const formattedDate = formatDate(event.target.value);
      setStartDate(formattedDate);
    } else {
      alert(
        "Invalid start date. Please select a date within the next 15 days."
      );
    }
  };

  const handleEndDateChange = (event: ChangeEvent<HTMLInputElement>) => {
    const selectedDate = new Date(event.target.value);
    const today = new Date();
    const maxAllowedDate = new Date();
    maxAllowedDate.setDate(today.getDate() + 15);

    if (selectedDate >= today && selectedDate <= maxAllowedDate) {
      const formattedDate = formatDate(event.target.value);
      setEndDate(formattedDate);
    } else {
      alert("Invalid end date. Please select a date within the next 15 days.");
    }
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();

    const selectedCityData = cities.find((city) => city.city === selectedCity);

    const newTrip = {
      id: selectedCityData ? selectedCityData.id : "",
      city: selectedCity,
      image: selectedCityImage,
      startDate: startDate,
      endDate: endDate,
    };

    onAddTrip(newTrip);
    onClose();
  };
  const resetState = () => {
    setSelectedCity("");
    setSelectedCityImage("");
    setStartDate("");
    setEndDate("");

    const startDateInput = document.getElementsByName(
      "startDate"
    )[0] as HTMLInputElement;
    const endDateInput = document.getElementsByName(
      "endDate"
    )[0] as HTMLInputElement;

    if (startDateInput) {
      startDateInput.value = "";
    }

    if (endDateInput) {
      endDateInput.value = "";
    }
  };

  const handleCancel = () => {
    resetState();
    onClose();
  };

  useEffect(() => {
    resetState();
  }, [isOpen]);

  return (
    <div>
      <div className="modal" hidden={!isOpen} style={modalStyle}>
        <div className="trip-popup">
          <h2 className="trip-popup__title">Create trip</h2>
          <button className="trip-popup__close" onClick={onClose}>
            ×
          </button>
          <form
            autoComplete="off"
            onSubmit={handleSubmit}
            onAbort={handleCancel}
          >
            <div className="input-container">
              <label className="input">
                <span className="input__heading">
                  <span className="star--red">*</span> City
                </span>
                <select
                  name="city"
                  value={selectedCity}
                  onChange={handleCityChange}
                  required
                >
                  <option value="" disabled>
                    Please select a city
                  </option>
                  {cities.map((city) => (
                    <option key={city.id} value={city.city}>
                      {city.city}
                    </option>
                  ))}
                </select>
              </label>
              <label className="input">
                <span className="input__heading">
                  <span className="star--red">*</span> Start date
                </span>
                <input
                  name="startDate"
                  type="date"
                  required
                  onChange={handleStartDateChange}
                />
              </label>
              <label className="input">
                <span className="input__heading">
                  <span className="star--red">*</span> End date
                </span>
                <input
                  className="input-field"
                  name="endDate"
                  type="date"
                  required
                  onChange={handleEndDateChange}
                />
              </label>
            </div>
            <div className="buttons-container">
              <button
                className="button button--gray"
                type="button"
                onClick={handleCancel}
              >
                Cancel
              </button>

              <button className="button button--blue" type="submit">
                Save
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
