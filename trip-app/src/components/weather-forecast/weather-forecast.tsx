import { FC } from "react";

interface IProps {
  weekday: string;
  icon: string | undefined;
  temperature: string;
}

export const WeatherForecast: FC<IProps> = ({ weekday, icon, temperature }) => {
  return (
    <div className="wheather-forecast-container">
      <p className="wheather-forecast-container__weekday">{weekday}</p>
      <div className="wheather-forecast-container__icon">
        <img src={icon} />
      </div>
      <p className="wheather-forecast-container__temperature">{temperature}</p>
    </div>
  );
};
