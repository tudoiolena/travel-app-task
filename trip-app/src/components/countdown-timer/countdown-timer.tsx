import { FC, useCallback, useEffect, useState } from "react";

interface IProps {
  startDate: string;
}

export const CountdownTimer: FC<IProps> = ({ startDate }) => {
  const calculateTimeLeft = useCallback(() => {
    const [day, month, year] = startDate.split(".");
    const startDateObject = new Date(`${year}-${month}-${day}`);
    const difference = startDateObject.getTime() - new Date().getTime();
    if (difference <= 0) {
      return { days: 0, hours: 0, minutes: 0, seconds: 0 };
    }

    const days = Math.floor(difference / (1000 * 60 * 60 * 24));
    const hours = Math.floor(
      (difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    const minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((difference % (1000 * 60)) / 1000);

    return { days, hours, minutes, seconds };
  }, [startDate]);

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft);

  useEffect(() => {
    const timer = setInterval(() => {
      setTimeLeft(calculateTimeLeft);
    }, 1000);

    return () => clearInterval(timer);
  }, [calculateTimeLeft, startDate]);

  return (
    <div className="timer-container">
      <p className="timer-content">
        <span className="timer-content__count-numbers">{timeLeft.days} </span>
        <span className="timer-content__title">DAYS</span>
      </p>
      <p className="timer-content">
        <span className="timer-content__count-numbers">{timeLeft.hours} </span>
        <span className="timer-content__title">HOURS</span>
      </p>
      <p className="timer-content">
        <span className="timer-content__count-numbers">{timeLeft.minutes}</span>
        <span className="timer-content__title">MINUTES</span>{" "}
      </p>
      <p className="timer-content">
        <span className="timer-content__count-numbers">{timeLeft.seconds}</span>
        <span className="timer-content__title">SECONDS</span>
      </p>
    </div>
  );
};
